(() => {
    /**
     * Check and set a global guard variable.
     * If this content script is injected into the same page again,
     * it will do nothing next time.
     */
    if (window.hasRun) {
        return;
    }
    window.hasRun = true;

    browser.storage.local.get("exps").then((result) => {
        const old_exps = result["exps"];
        if (old_exps !== undefined) {
            document.getElementById("lt-exps").value = old_exps;
        }
    });

    function save_data(_ev) {
        let exps = document.getElementById("lt-exps").value;
        browser.storage.local.set({ "exps": exps });
        window.close();
    }

    document.getElementById("submit-btn").addEventListener("click", save_data);
})();
