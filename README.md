### Link transformer

This extension allows you to specify systematic modifications to URLs when
opening a link.

For example, if you prefer to run web searches with duckduckgo.com instead of
google.com and prefer to access twitter content through the nitter.net frontend,
you can write rules to substitute them:

```
google\.com|duckduckgo.com
twitter\.com|nitter.net
```

(The part to the left of the `|` is a regular expression, so remember to
backslash-escape special characters. Add the rules to the textbox that pops up
when you click on the extension icon from the toolbar.)

Now when right-clicking on a google search link you will have the option
("Transform link") to open the corresponding duckduckgo site instead. You can
also go to the corresponding page while on a site in the google.com domain
(right-click on the page, i.e., not on a link/picture/etc., then "Go to
transform of current page").

Rules are regular expressions on the left of the `|` and strings on the right,
and will be applied in sequence by Javascript's String.prototype.replace method.
Therefore more complicated substitutions are possible. For example

```
example\.com\/(\w*)\/(\w*)\?q=(\w*)|example.com/$2/$1?p=$3
```
will use capturing groups to reorder the parts of the url.
