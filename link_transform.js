function apply_transform_rules(url) {
    if (url === undefined) {
        return;
    }

    let newUrl = url;
    return browser.storage.local.get("exps").then((result) => {
        let exps = result["exps"];
        if (exps === undefined) { // if user hasn't set anything
            return url;
        }

        let list = exps.split("\n");
        for (let l of list) {
            let phrases = l.split("|");
            if (phrases.length !== 2) {
                console.log("Link transform: each rule must consist of <exp1>|<exp2> where exp1 can make a regexp and exp2 can be provided as a string argument to String.prototype.replace; skipping provided rule " + l);
                continue;
            }
            try {
                let re = new RegExp(phrases[0]);
                newUrl = newUrl.replace(re, phrases[1]);
            }
            catch (_e) {
                console.log("Link transform couldn't parse or use rule " + l + ", skipping");
            }
        }
        return newUrl;
    });
}

// Callback reads runtime.lastError to prevent an unchecked error from being
// logged when the extension attempt to register the already-registered menu
// again. Menu registrations in event pages persist across extension restarts.
browser.contextMenus.create({
    id: "link_transform_link",
    title: "Transform link",
    contexts: ["link"],
},
    // See https://extensionworkshop.com/documentation/develop/manifest-v3-migration-guide/#event-pages-and-backward-compatibility
    // for information on the purpose of this error capture.
    () => void browser.runtime.lastError,
);

browser.contextMenus.create({
    id: "link_transform_current",
    title: "Go to transform of current page",
    contexts: ["page"],
},
    // See https://extensionworkshop.com/documentation/develop/manifest-v3-migration-guide/#event-pages-and-backward-compatibility
    // for information on the purpose of this error capture.
    () => void browser.runtime.lastError,
);

browser.contextMenus.onClicked.addListener((info, _tab) => {
    if (info.menuItemId !== "link_transform_link") {
        return;
    }

    apply_transform_rules(info.linkUrl).then((newUrl) => {
        browser.tabs.create({
            active: false,
            url: newUrl
        }
        );

    });
});

browser.contextMenus.onClicked.addListener((info, tab) => {
    if (info.menuItemId !== "link_transform_current") {
        return;
    }
    apply_transform_rules(tab.url)
        .then((newUrl) => {
            if (tab.url !== newUrl) {
                browser.tabs.update(
                    { "url": newUrl }
                )
            }
        });
});
